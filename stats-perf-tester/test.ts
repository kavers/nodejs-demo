import * as path from 'path';
import fc from 'fast-check';
import Jasmine = require('jasmine');


fc.configureGlobal({endOnFailure: true});

//jasmine всегда трактует specDir как относительный путь к текущей рабочей директории.
const specDir = path.join(path.relative(process.cwd(), __dirname), 'app');

const jasmine = new Jasmine({});
jasmine.loadConfig({
    'spec_dir': specDir,
    'spec_files': [
        '**/*.spec.ts'
    ],
    'stopSpecOnExpectationFailure': true,
    'random': true,
    'helpers': [
        '../helpers/**/*.ts'
    ]
});

jasmine.execute();
