import * as yargs from 'yargs';
import {inRange, flow} from 'lodash';
import {readFileSync} from 'fs';
import * as path from 'path';
import * as Ajv from 'ajv';

import {
    StatsTables,
    statsPerformanceTester
} from './app';


function cli(): void {
    statsPerformanceTester(parseArgs(process.argv), process.stdout)
        .catch(err => console.error(`Ошибка: ${(err as Error).message}`));
}

function parseArgs(argv: string[]): CliArguments {
    return yargs(argv.splice(2))
        .usage('Использование: $0 [options]')
        .option('host', {
            alias: 'h',
            describe: 'Адрес сервера СУБД',
            type: 'string',
            default: 'localhost'
        })
        .option('port', {
            describe: 'Порт для подключения к СУБД',
            type: 'number',
            default: 8123,
            coerce: tcpPort
        })
        .option('user', {
            alias: 'u',
            describe: 'Пользователь для подключения к СУБД',
            type: 'string',
            default: 'default'
        })
        .option('password', {
            alias: 'p',
            describe: 'Пароль для подключения к СУБД',
            type: 'string',
            default: ''
        })
        .option('database', {
            alias: 'db',
            describe: 'Имя базы данных, на которой будут проведены измерения',
            type: 'string',
            demandOption: 'Пожалуйста, укажите базу данных для работы'
        })
        .option('tables', {
            describe: 'Путь к json-файлу с описанием используемых аналитических таблиц в БД',
            demandOption: 'Пожалуйста, укажите путь к файлу с описанием используемых таблиц',
            coerce: flow([loadStatTablesConfig, notEmpty])
        })
        .strict()
        .help()
        .argv;
}

function loadStatTablesConfig(configPath: string): StatsTables {
    const config = JSON.parse(readFileSync(configPath, 'utf8')) as Record<string, unknown>[] | Record<string, unknown>;
    const schema = JSON.parse(
        readFileSync(
            path.join(__dirname, 'assets', 'schemas', 'stats-tables.json'),
            'utf8'
        )
    ) as Record<string, unknown>;
    const ajv = new Ajv();

    if (!ajv.validate(schema, config)) {
        throw new TypeError(`Некорректный конфиг таблиц ${configPath}. Исходная ошибка: ${ajv.errorsText()}`);
    }

    return config as unknown as StatsTables;
}

function tcpPort(val: number): number {
    const min = 0;
    const max = 65535;
    if (!Number.isInteger(val) || !inRange(val, min, max)) {
        throw new TypeError(`Ожидается целое число от ${min} до ${max}.`);
    }

    return val;
}

function notEmpty<T>(val: T[]): T[] {
    if (val.length === 0) {
        throw new Error('Для работы необходим хотя бы один элемент в списке.');
    }

    return val;
}

interface CliArguments {
    host: string;
    port: number;
    user: string;
    password: string;
    database: string;
    tables: StatsTables;
}

cli();
