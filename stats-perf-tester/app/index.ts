export {statsPerformanceTester} from './app';
export {StatsTables} from './types';

export const reportNames = ['by-metric', 'by-group', 'with-time-slots'] as const;
export type ReportName = typeof reportNames[number];
