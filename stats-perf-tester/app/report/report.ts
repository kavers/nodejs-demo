/* eslint-disable @typescript-eslint/no-explicit-any */
import {unzip} from 'lodash';
import {concat, from, Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';


export function makePerfReport<T extends readonly any[]>(
    funcsLabel: string,
    funcTuples: readonly FuncTuple<T>[],
    argsTuples: readonly ArgsTuple<T>[],
    groupMeasurer: (funcs: Func<T>[], argsArr: T[]) => Observable<number>[]
): Observable<string>[] {
    const [funcLabels, funcs] = unzip(funcTuples) as [string[], Func<T>[]];
    const [argsLabels, argsArr] = unzip(argsTuples) as [string[], T[]];

    const reportHead$ = from([funcsLabel, ...argsLabels]);
    const reportRows$ = groupMeasurer(funcs, argsArr).map(
        (measures$, funcNum) => concat(
            of(funcLabels[funcNum]),
            measures$.pipe(
                map(periodInNanoseconds => Math.floor(periodInNanoseconds / 1e6).toString())
            )
        )
    );

    return [reportHead$, ...reportRows$];
}

type Func<T extends readonly any[]> = (...arg: T) => Promise<any>;
type FuncTuple<T extends readonly any[]> = readonly [label: string, func: Func<T>];
type ArgsTuple<T extends readonly any[]> = readonly [label: string, args: T];
