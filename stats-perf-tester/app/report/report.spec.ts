/* eslint-disable @typescript-eslint/no-explicit-any */
import fc from 'fast-check';
import {fill, tail, unzip} from 'lodash';
import {first, skip, toArray} from 'rxjs/operators';

import {makePerfReport} from './report';
import {from, Observable} from 'rxjs';


describe('Функции по созданию отчётов производительности', () => {
    describe(
        'makePerfReport - создаёт отчёт по времени выполнения в миллисекундах набора функций с одним и набором параметров',
        () => {
            const funcTupleArbitrary = fc.tuple(fc.string(1, 10), fc.func(
                fc.constant(new Promise(resolve => resolve())))
            );
            const funcTuplesArbitrary = fc.array(funcTupleArbitrary, 1, 10);
            const argsTupleArbitrary = fc.tuple(fc.string(1, 10), fc.array(fc.integer()));
            const argsTuplesArbitrary = fc.array(argsTupleArbitrary, 1, 10);

            const makeMeasurer = (resMeasure: number) => {
                return (
                    f: readonly any[],
                    args: readonly any[]
                ): Observable<number>[] => f.map(() => from(fill(Array(args.length), resMeasure)));
            }

            it('Пропустит ошибку от измерителя выше', () => {
                fc.assert(
                    fc.property(
                        funcTuplesArbitrary,
                        argsTuplesArbitrary,
                        fc.string(1, 10),
                        fc.string(1, 10),
                        (funcs, funcArgs, reportLabel, errorMsg) => {
                            const errorMeasurer = jasmine.createSpy('Spy Error').and.throwError(errorMsg);

                            expect(() => makePerfReport(reportLabel, funcs, funcArgs, errorMeasurer))
                                .toThrowError(errorMsg);
                        }
                    )
                );
            });

            it('Первой строкой отчёта идут заголовки', async () => {
                await fc.assert(
                    fc.asyncProperty(
                        funcTuplesArbitrary,
                        argsTuplesArbitrary,
                        fc.string(1, 10),
                        async (funcTuples, argsTuples, reportLabel) => {
                            const measurer = makeMeasurer(1);
                            const [argsLabels] = unzip(argsTuples);

                            const headRow$ = makePerfReport(reportLabel, funcTuples, argsTuples, measurer)[0];

                            await expectAsync(headRow$.pipe(toArray()).toPromise())
                                .toBeResolvedTo([reportLabel, ...argsLabels as string[]]);
                        }
                    )
                );
            });

            it('Первой колонкой отчёта идут имена функций', async () => {
                await fc.assert(
                    fc.asyncProperty(
                        funcTuplesArbitrary,
                        argsTuplesArbitrary,
                        fc.string(1, 10),
                        async (funcs, funcArgs, reportLabel) => {
                            const measurer = makeMeasurer(1);
                            const [funcLabels] = unzip(funcs);

                            const labels = [];
                            for (const row$ of makePerfReport(reportLabel, funcs, funcArgs, measurer)) {
                                labels.push(await row$.pipe(first()).toPromise());
                            }

                            expect(labels).toEqual([reportLabel, ...funcLabels as string[]]);
                        }
                    )
                );
            });

            it('Замеры из измерителя в наносекундах переводятся в миллисекунды', async () => {
                await fc.assert(
                    fc.asyncProperty(
                        funcTuplesArbitrary,
                        argsTuplesArbitrary,
                        fc.string(1, 10),
                        async (funcTuples, argsTuples, reportLabel) => {
                            const periodInNanoseconds = 1e8;
                            const measurer = makeMeasurer(periodInNanoseconds);
                            const measureRows = tail(makePerfReport(reportLabel, funcTuples, argsTuples, measurer)).map(
                                row$ => row$.pipe(
                                    skip(1),
                                    toArray()
                                )
                            );
                            const measures = [];
                            for (const row$ of measureRows) {
                                measures.push([...await row$.toPromise()]);
                            }

                            expect(measures).toEqual(fill(
                                Array(funcTuples.length),
                                fill(Array(argsTuples.length), (Math.floor(periodInNanoseconds / 1e6)).toString())
                            ));
                        }
                    )
                );
            });
        }
    );
});
