/* eslint-disable @typescript-eslint/no-explicit-any */
import fc from 'fast-check';
import {fill, sum, range} from 'lodash';
import {ElementOf} from 'ts-essentials';
import {toArray} from 'rxjs/operators';

import {groupMeasures, measures, avgMeasure} from './group';


describe('Функции для организации серии измерений времени выполнения', () => {
    const funcArbitrary = fc.integer().map(
        val => {
            return (..._args: number[]) => new Promise(resolve => resolve(val));
        }
    );
    const funcsArbitrary = fc.array(funcArbitrary, 1, 10);
    const argsArbitrary = fc.array(fc.array(fc.integer(), 1, 3), 1, 10);

    describe('avgMeasure - измерение среднего времени выполнения функции', () => {
        const countArbitrary = fc.integer(1, 10);

        it('Падение измерителя пробрасывается наружу', async () => {
            await fc.assert(
                fc.asyncProperty(
                    funcArbitrary,
                    countArbitrary,
                    fc.string(1, 10),
                    async (func, count, errorMsg) => {
                        const errorSpy = jasmine.createSpy('Spy Error').and.throwError(errorMsg);
                        await expectAsync(avgMeasure(func, [], count, errorSpy)).toBeRejectedWithError(errorMsg);
                    }
                )
            );
        });

        it('Измеритель вызывается count раз c переданным аргументом и функцией', async () => {
            await fc.assert(
                fc.asyncProperty(
                    funcArbitrary,
                    countArbitrary,
                    fc.integer(1, 20),
                    async (func, count, arg) => {
                        const spyCounter = jasmine.createSpy('Spy Counter').and.resolveTo();

                        await avgMeasure(func, [arg], count, spyCounter);
                        expect(spyCounter.calls.count()).toBe(count);
                        expect(spyCounter.calls.allArgs()).toEqual(fill(Array(count), [func, [arg]]));
                    }
                )
            );
        });

        it('Возвращает среднее от значений, возвращаемых измерителем', async () => {
            const measuredArbitrary = fc.array(fc.nat(), 1, 100);

            await fc.assert(
                fc.asyncProperty(
                    funcArbitrary,
                    measuredArbitrary,
                    fc.integer(1, 20),
                    async (func, measured, arg) => {
                        let callCount = 0;
                        const spyCounter = (..._args: any[]): Promise<number> => new Promise(resolve => {
                            resolve(measured[callCount++]);
                        });

                        await expectAsync(avgMeasure(func, [arg], measured.length, spyCounter))
                            .toBeResolvedTo(Math.floor(sum(measured) / measured.length));
                    }
                )
            );
        });
    });

    describe('measures - делает серию измерений одной функции с разными аргументами', () => {
        it('Падение измеряющей функции пробрасывается наружу', async () => {
            await fc.assert(
                fc.asyncProperty(
                    funcArbitrary,
                    argsArbitrary,
                    fc.string(1, 10),
                    async (func, argsArr, errorMsg) => {
                        const errorSpy = jasmine.createSpy('Spy Error').and.throwError(errorMsg);

                        await expectAsync(measures(func, argsArr, errorSpy).toPromise())
                            .toBeRejectedWithError(errorMsg);
                    }
                )
            );
        });

        it('В измеряющую функцию передаётся именно измеряемая', async () => {
            await fc.assert(
                fc.asyncProperty(
                    funcArbitrary,
                    argsArbitrary,
                    async (func, argsArr) => {
                        const measurerSpy = jasmine.createSpy('Spy Measurer').and.resolveTo();
                        await measures(func, argsArr, measurerSpy).toPromise();
                        const firstArgs = measurerSpy.calls.allArgs().map(args => args[0] as typeof func);

                        expect(firstArgs).toEqual(fill(Array(argsArr.length), func));
                    }
                )
            );
        });

        it('Измеряющая функция вызывается с каждым поднабором аргументов', async () => {
            await fc.assert(
                fc.asyncProperty(
                    funcArbitrary,
                    argsArbitrary,
                    async (func, argsArr) => {
                        const measurerSpy = jasmine.createSpy('Spy Measurer').and.resolveTo();
                        await measures(func, argsArr, measurerSpy).toPromise();
                        const usedArgs = measurerSpy.calls.allArgs().map(args => args[1] as ElementOf<typeof argsArr>);

                        expect(usedArgs).toEqual(argsArr);
                    }
                )
            );
        });

        it('Последовательность результатов измерений соответствует последовательности аргументов', async () => {
            await fc.assert(
                fc.asyncProperty(
                    funcArbitrary,
                    argsArbitrary,
                    async (func, argsArr) => {
                        const measurerSpy = jasmine.createSpy('Spy Measurer');
                        for (const args of argsArr) {
                            measurerSpy.withArgs(func, args).and.resolveTo(args[0]);
                        }
                        await expectAsync(measures(func, argsArr, measurerSpy).pipe(toArray()).toPromise())
                            .toBeResolvedTo(argsArr.map(args => args[0]));
                    }
                )
            );
        });
    });

    describe('groupMeasures - готовит серию измерений нескольких функций на общем наборе аргументов', () => {
        it('Падение измерителя пробрасывается наружу', () => {
            fc.assert(
                fc.property(
                    funcsArbitrary,
                    argsArbitrary,
                    fc.string(1, 10),
                    (funcs, argsArr, errorMsg) => {
                        const errorSpy = jasmine.createSpy('Spy Error').and.throwError(errorMsg);

                        expect(() => groupMeasures(funcs, argsArr, errorSpy)).toThrowError(errorMsg);
                    }
                )
            );
        });

        it('В измеритель передаётся каждая измеряемая функция c аргументами', () => {
            fc.assert(
                fc.property(
                    funcsArbitrary,
                    argsArbitrary,
                    (funcs, argsArr) => {
                        const measurerSpy = jasmine.createSpy('Spy Measurer');
                        groupMeasures(funcs, argsArr, measurerSpy);
                        expect(measurerSpy.calls.allArgs()).toEqual(funcs.map(func => [func, argsArr]));
                    }
                )
            );
        });

        it('Последовательность соответствует последовательности функций', () => {
            fc.assert(
                fc.property(
                    funcsArbitrary,
                    argsArbitrary,
                    (funcs, argsArr) => {
                        const measurerSpy = jasmine.createSpy('Spy Measurer');
                        let callCount = 0;
                        for (const func of funcs) {
                            measurerSpy.withArgs(func, argsArr).and.returnValue(callCount++);
                        }

                        expect(groupMeasures(funcs, argsArr, measurerSpy) as unknown as number[])
                            .toEqual(range(0, callCount));
                    }
                )
            );
        });
    });
});
