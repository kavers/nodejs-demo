/* eslint-disable @typescript-eslint/no-explicit-any */
import {from, Observable} from 'rxjs';
import {concatMap} from 'rxjs/operators';
import {sum} from 'lodash';

import {Func} from './types';


export async function avgMeasure<T extends readonly any[]>(
    func: Func<T>,
    args: T,
    count: number,
    measurer: (f: Func<T>, a: T) => Promise<number>
): Promise<number> {
    const measures: number[] = [];
    for (let i = 0; i < count; i++) {
        measures.push(await measurer(func, args));
    }

    return Math.floor(sum(measures) / measures.length);
}

export function measures<T extends readonly any[]>(
    func: Func<T>,
    argsArr: readonly T[],
    measurer: (f: Func<T>, args: T) => Promise<number>
): Observable<number> {
    return from(argsArr).pipe(
        concatMap(args => from(measurer(func, args)))
    );
}

export function groupMeasures<T extends readonly any[]>(
    funcs: Func<T>[],
    argsArr: readonly T[],
    groupMeasurer: (f: Func<T>, argsArr: readonly T[]) => Observable<number>
): Observable<number>[] {
    return funcs.map(func => groupMeasurer(func, argsArr));
}
