/* eslint-disable @typescript-eslint/no-explicit-any */
import {partialRight} from 'lodash';

import {groupMeasures, measures, avgMeasure} from './group';
import {measure} from './measure';
import {Func} from './types';
import {Observable} from 'rxjs';

const avgMeasureInNanoseconds = partialRight(avgMeasure, 10, measure);
const measuresInNanoseconds = partialRight(measures, avgMeasureInNanoseconds);
export const groupAvgMeasureInNanoseconds: GroupMeasurer = partialRight(groupMeasures, measuresInNanoseconds);

type GroupMeasurer<T extends readonly any[] = readonly any[]> = (funcs: Func<T>[], argsArr: T[]) => Observable<number>[]
