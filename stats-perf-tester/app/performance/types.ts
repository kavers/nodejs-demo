/* eslint-disable @typescript-eslint/no-explicit-any */
export type Func<T extends readonly any[]> = (...arg: T) => Promise<any>;
