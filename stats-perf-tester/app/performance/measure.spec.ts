import fc from 'fast-check';

import {measure} from './measure';


describe('Функции для измерения длительности выполнения кода', () => {
    const sleep = async (period: number): Promise<undefined> => {
        return new Promise(resolve => {
            setTimeout(resolve, period);
        });
    };

    const periodArbitrary = fc.integer(100, 200);

    describe('measure - измерение времени выполнения функции', () => {
        it('Падение измеряемой функции пробрасывается наружу', async () => {
            await fc.assert(
                fc.asyncProperty(
                    fc.string(1, 10),
                    async errorMsg => {
                        const errorSpy = jasmine.createSpy('Spy Error').and.throwError(errorMsg);
                        await expectAsync(measure(errorSpy, [])).toBeRejectedWithError(errorMsg);
                    }
                )
            );
        });

        it('Исследуемая функция вызывается один раз с переданными аргументами', async () => {
            await fc.assert(
                fc.asyncProperty(periodArbitrary, async period => {
                    const spyCounter = jasmine.createSpy('Spy Counter').and.resolveTo();

                    await measure(spyCounter, [period]);
                    expect(spyCounter.calls.count()).toBe(1);
                    expect(spyCounter.calls.mostRecent().args).toEqual([period]);
                })
            );
        });

        it('Длительность выполнения функции определяется с точность 80%', async () => {
            await fc.assert(
                fc.asyncProperty(periodArbitrary, async period => {
                    const measured = await measure(sleep, [period]);
                    expect(measured).toBeLessThanOrEqual(period * 1e6 * 1.2);
                    expect(measured).toBeGreaterThanOrEqual(period * 1e6 * 0.8);
                }), {numRuns: 3} // Чтобы проверки не занимали слишком много времени
            );
        });
    });
});
