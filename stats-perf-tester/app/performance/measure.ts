/* eslint-disable @typescript-eslint/no-explicit-any */
import {Func} from './types';


export async function measure<T extends readonly any[]>(func: Func<T>, args: T): Promise<number> {
    const NS_PER_SEC = 1e9;
    const start = process.hrtime();
    await func(...args);
    const period = process.hrtime(start);
    return period[0] * NS_PER_SEC + period[1];
}

