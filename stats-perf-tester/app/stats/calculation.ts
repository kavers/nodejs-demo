import {Observable} from 'rxjs';

import {
    Aggregation,
    DateRange,
    DimensionExp,
    DimensionType,
    MeasureExp,
} from './types';


export function makeStatsCalc<Field extends string, Alias extends string>(
    select: SelectFunc<Field>,
    measures: ReadonlyMap<Alias, MeasureExp<Field>>
): CalcFunc<Field, Alias> {
    return (dimensions, groupBy?) => {
        return select(
            measuresToFields(measures),
            dimensionsToFilters(dimensions),
            groupBy ? [{field: groupBy, order: 'asc'}] : undefined,
            groupBy ? [groupBy] : undefined
        );
    };
}

interface CalcFunc<F extends string, A extends string> {
    (dimensions: readonly DimensionExp<F>[], groupBy?: F): Observable<Row<A>>;
}

function measuresToFields<F extends string, A extends string>(
    measures: ReadonlyMap<A, MeasureExp<F>>
): FieldExp<F, A>[] {
    return [...measures.entries()].map(([alias, measure]) => {
        return {
            alias,
            name: measure.field,
            exp: measure.exp
        };
    });
}

function isSet(value: Readonly<DimensionType>): value is Set<number | string> {
    return value instanceof Set;
}

function dimensionsToFilters<F extends string>(
    dimensions: readonly DimensionExp<F>[]
): FilterExp<F>[] {
    return dimensions.map(dimension => {
        if (isSet(dimension.value)) {
            return {
                field: dimension.field,
                value: [...dimension.value.values()],
                comparator: dimension.fieldType === 'single' ? 'in' : 'hasAny'
            }
        } else {
            return {
                field: dimension.field,
                value: dimension.value,
                comparator: 'between'
            }
        }
    });
}

export interface SelectFunc<F extends string> {
    <Alias extends string>(
        fields: readonly FieldExp<F, Alias>[],
        filters: readonly FilterExp<F>[],
        orders?: readonly {field: F; order: 'asc' | 'desc'}[],
        groupBy?: readonly F[]
    ): Observable<Row<Alias>>;
}

interface FieldExp<F extends string, A extends string> {
    readonly alias: A;
    readonly name: F;
    readonly exp: Aggregation;
}

interface Filter<F extends string, C extends string, T extends readonly (string | number)[] | DateRange> {
    readonly field: F;
    readonly comparator: C;
    readonly value: T;
}

type FilterExp<F extends string> =
    Filter<F, 'in' | 'hasAny', readonly (string | number)[]>
    | Filter<F, 'between', DateRange>;

type Row<F extends string> = Record<F, unknown>;
