import fc from 'fast-check';
import {map} from 'lodash';

import {randMeasures} from './measure';
import {typeToAggregation, DataType} from './types';


describe('Функции для работы с метриками', () => {
    describe('randMeasures - возвращает случайные метрики на основе типа полей', () => {
        const fieldArbitrary = fc.tuple(
            fc.string(1, 10),
            fc.constantFrom(...Object.keys(typeToAggregation) as DataType[])
        );

        it('Возвращает метрики для всех переданных полей', () => {
            fc.assert(
                fc.property(fc.array(fieldArbitrary, 10), fields => {
                    const fieldsMap = new Map(fields);
                    const measures = randMeasures(fieldsMap);

                    expect(map(measures, 'field').sort()).toEqual([...fieldsMap.keys()].sort());
                })
            );
        });

        it('Тип агрегации соответствует типу поля', () => {
            fc.assert(
                fc.property(fc.array(fieldArbitrary, 10), fields => {
                    const fieldsMap = new Map(fields);
                    const measures = randMeasures(fieldsMap);

                    for (const {field, exp} of measures) {
                        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                        expect(typeToAggregation[fieldsMap.get(field)!]).toContain(exp);
                    }
                })
            );
        });
    });
});
