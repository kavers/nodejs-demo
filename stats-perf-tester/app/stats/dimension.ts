import {random, sampleSize} from 'lodash';

import {DateRange, DimensionExp, DimensionType} from './types';


export function randSubDimensions<DateField extends string, DimensionField extends string>(
    dateDimension: DimensionExp<DateField, DateRange>,
    otherDimensions: readonly DimensionExp<DimensionField>[],
    periodInSeconds: number
): DimensionExp<DimensionField | DateField>[] {
    const subDimensions: DimensionExp<DimensionField | DateField>[] = [];

    if (otherDimensions.find(other => other.field === dateDimension.field as unknown as DimensionField)) {
        throw new TypeError(`Поле dateDimension "${dateDimension.field}" не должно встречаться в otherDimensions`);
    }

    subDimensions.push({ ...dateDimension, value: randSubRange(dateDimension.value, periodInSeconds)});

    for (const other of otherDimensions) {
        if (isDateRange(other.value)) {
            subDimensions.push({...other, value: randSubRange(other.value)});
        } else {
            subDimensions.push({...other, value: randSubSet(other.value)});
        }
    }

    return subDimensions;
}

function isDateRange(val: DimensionType): val is DateRange {
    return 'min' in val;
}

function randSubRange(range: DateRange, periodInSeconds?: number): DateRange {
    const rangePeriodInSeconds = toSeconds(range.max.getTime() - range.min.getTime());
    if (periodInSeconds !== undefined && rangePeriodInSeconds < periodInSeconds) {
        throw new RangeError(`Period "${periodInSeconds}" have to be less than the date range "${rangePeriodInSeconds}"`);
    }

    const targetPeriodInSeconds = periodInSeconds === undefined ? random(rangePeriodInSeconds) : periodInSeconds;
    const startSubRange = new Date(
        range.min.getTime() + toMilliseconds(random(rangePeriodInSeconds - targetPeriodInSeconds))
    );
    const endSubRange = new Date(
        startSubRange.getTime() + toMilliseconds(targetPeriodInSeconds)
    );

    return {
        min: startSubRange,
        max: endSubRange
    };
}

function randSubSet<T extends number | string>(val: ReadonlySet<T>): Set<T> {
    return new Set(
        sampleSize([...val.values()], random(val.size))
    );
}

function toSeconds(milliseconds: number): number {
    return Math.floor(milliseconds / 1000);
}

function toMilliseconds(seconds: number): number {
    return seconds * 1000;
}
