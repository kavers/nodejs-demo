import fc from 'fast-check';
import {random, difference, map} from 'lodash';

import {randSubDimensions} from './dimension';
import {DateRange} from './types';


describe('Функции для работы с измерениями в аналитических отчётах', () => {
    describe(
        'randSubDimensions - возвращает случайный под-измерения с заданным периодом по измерению времени',
        () => {
            const dateRangeArbitrary = fc.tuple(
                fc.date({min: new Date('1980-01-01'), max: new Date()}),
                fc.integer(0, 1000000)
            ).map(([startDate, periodInSeconds]) => ({
                min: startDate,
                max: new Date(startDate.getTime() + periodInSeconds * 1000)
            }));

            const setArbitrary = fc.oneof(
                fc.array(fc.integer(), 1000),
                fc.array(fc.string(1, 10), 1000)
            ).map(arr => new Set<string | number>(arr));

            const fieldTypes = ['single', 'array'] as const;
            const dimensionExpArbitrary = fc.record({
                field: fc.string(1, 20),
                fieldType: fc.constantFrom(...fieldTypes),
                value: fc.oneof(
                    dateRangeArbitrary,
                    setArbitrary
                )
            });

            const argsArbitrary = fc.record({
                dateDimension: fc.record({field: fc.string(1, 20), fieldType: fc.constantFrom(...fieldTypes), value: dateRangeArbitrary}),
                otherDimensions: fc.array(dimensionExpArbitrary, 1, 10)
            }).filter(
                ({dateDimension, otherDimensions}) => !otherDimensions.find(
                    other => other.field === dateDimension.field
                )
            );

            const randSubPeriod = ({min, max}: DateRange): number => Math.floor(
                (random(max.getTime() - min.getTime())) / 1000
            );

            it('Падает, если период больше данного диапазона', () => {
                fc.assert(
                    fc.property(argsArbitrary, ({dateDimension, otherDimensions}) => {
                        const {min, max} = dateDimension.value;
                        const bigPeriod = (max.getTime() - min.getTime()) * random(1, 5, true) + 1000;
                        expect(() => randSubDimensions(
                            dateDimension,
                            otherDimensions,
                            Math.floor(bigPeriod / 1000)
                        )).toThrow();
                    })
                );
            });

            it('Падает, если поле измерение времени есть и в otherDimensions', () => {
                fc.assert(
                    fc.property(argsArbitrary, ({dateDimension, otherDimensions}) => {
                        expect(() => randSubDimensions(
                            {...dateDimension, field: otherDimensions[0].field},
                            otherDimensions,
                            randSubPeriod(dateDimension.value)
                        )).toThrow();
                    })
                );
            });

            it('Диапазон результата совпадает с заданным периодом', () => {
                fc.assert(
                    fc.property(argsArbitrary, ({dateDimension, otherDimensions}) => {
                        const subPeriod = randSubPeriod(dateDimension.value);
                        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                        const subRange = randSubDimensions(
                            dateDimension,
                            otherDimensions,
                            subPeriod
                        ).find(dim => dim.field === dateDimension.field)!.value as DateRange;
                        expect(
                            Math.floor((subRange.max.getTime() - subRange.min.getTime()) / 1000)
                        ).toBe(subPeriod);
                    })
                );
            });

            it('Используются все данные измерения', () => {
                fc.assert(
                    fc.property(argsArbitrary, ({dateDimension, otherDimensions}) => {
                        const argFields = [dateDimension.field, ...map(otherDimensions, 'field')];
                        const subFields = map(randSubDimensions(
                            dateDimension,
                            otherDimensions,
                            randSubPeriod(dateDimension.value)
                        ), 'field');
                        expect(subFields.sort()).toEqual(argFields.sort());
                    })
                );
            });

            it('Значения под-измерений взяты из соответствующих исходных', () => {
                fc.assert(
                    fc.property(argsArbitrary, ({dateDimension, otherDimensions}) => {
                        const argDims = [dateDimension, ...otherDimensions];
                        const subDims = randSubDimensions(
                            dateDimension,
                            otherDimensions,
                            randSubPeriod(dateDimension.value)
                        );
                        const isBetween = (
                            date: Date,
                            range: DateRange
                        ): boolean => date >= range.min && date <= range.max;

                        for (const {field, value} of subDims) {
                            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                            const argDim = argDims.find(dim => dim.field === field)!;
                            if (value instanceof Set) {
                                expect(
                                    difference(
                                        [...value.values()],
                                        [...(argDim.value as Set<string | number>).values()]
                                    )
                                ).toEqual([]);
                            } else {
                                expect(isBetween(value.min, argDim.value as DateRange)).toBeTrue();
                                expect(isBetween(value.max, argDim.value as DateRange)).toBeTrue();
                                expect(value.min <= value.max).toBeTrue();
                            }
                        }
                    })
                );
            });
        }
    );
});
