import {sample} from 'lodash';

import {DataType, MeasureExp, typeToAggregation} from './types';


export function randMeasures<F extends string>(fields: ReadonlyMap<F, DataType>): MeasureExp<F>[] {
    return [...fields.entries()].map(
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        ([name, type]) => ({field: name, exp: sample(typeToAggregation[type])!})
    );
}
