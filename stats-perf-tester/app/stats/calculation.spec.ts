import fc from 'fast-check';
import {Observable} from 'rxjs';

import {makeStatsCalc} from './calculation';


describe('Функции для получения аналитических данных из таблиц', () => {
    describe('makeStatsCalc - создаёт функцию для запросов измерений из переданной таблицы', () => {
        const aggregations = ['sum', 'avg', 'std', 'min', 'max', 'count'] as const;
        const measureExpArbitrary = fc.record({
            field: fc.string(1, 10),
            exp: fc.constantFrom(...aggregations)
        });
        const aliasedMeasureExpArbitrary = fc.tuple(
            fc.string(1, 10),
            measureExpArbitrary
        );
        const dimensionValueArbitrary = fc.oneof(
            fc.record({min: fc.date(), max: fc.date()}),
            fc.set(fc.oneof(fc.string(1, 10), fc.integer()), 1, 20).map(values => new Set(values))
        );
        const fieldTypes = ['single', 'array'] as const;
        const dimensionExpArbitrary = fc.record({
            field: fc.string(1, 10),
            fieldType: fc.constantFrom(...fieldTypes),
            value: dimensionValueArbitrary
        });

        it('Отрабатывает при валидных параметрах', () => {
            fc.assert(
                fc.property(fc.array(aliasedMeasureExpArbitrary, 1, 20), exps => {
                    const select = (): Observable<Record<string, unknown>> => new Observable();

                    expect(makeStatsCalc(select, new Map(exps))).toBeDefined();
                })
            );
        });

        it('Полученная функция падает, если падает переданный select', () => {
            fc.assert(
                fc.property(
                    fc.array(aliasedMeasureExpArbitrary, 1, 20),
                    fc.array(dimensionExpArbitrary, 1, 20),
                    fc.string(1, 10),
                    (measures, dimensions, groupBy) => {
                        const select = (): never => { throw new Error('Error!'); };
                        const calc = makeStatsCalc(select, new Map(measures));
                        expect(() => calc(dimensions, groupBy)).toThrowError('Error!');
                    }
                )
            );
        });
    });
});
