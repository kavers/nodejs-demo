export interface MeasureExp<Field extends string> {
    readonly field: Field;
    readonly exp: Aggregation;
}

export interface DimensionExp<Field extends string, T extends DimensionType = DimensionType> {
    readonly field: Field;
    readonly fieldType: 'single' | 'array';
    readonly value: T;
}

export type DimensionType = DateRange | Set<number | string>;
export interface DateRange {
    min: Date;
    max: Date;
}

export const typeToAggregation = {
    key: ['count'],
    number: ['sum', 'avg', 'std', 'min', 'max']
} as const;
export type DataType = keyof typeof typeToAggregation;
export type Aggregation = typeof typeToAggregation[DataType][number];

