import {from, zip} from 'rxjs';
import {first, map, min, switchMap, tap, toArray} from 'rxjs/operators';
import {DeepReadonly} from 'ts-essentials';
import {sample} from 'lodash';
import WritableStream = NodeJS.WritableStream;

import {makeQueryFunc, checkDb, checkTable, makeSelectFunc} from './dbs/clickhouse';
import {loadSources, QueryFunc} from './dbs';
import {makeStatsCalc, randMeasures, randSubDimensions} from './stats';
import {StatsTable, Arguments, DbArguments, DateRange} from './types';
import {makePerfReport} from './report';
import {groupAvgMeasureInNanoseconds} from './performance';


export async function statsPerformanceTester(args: DeepReadonly<Arguments>, output: WritableStream): Promise<void> {
    await checkConnection(args);
    const queryFunc = makeQueryFunc(args.host, args.port, args.user, args.password, args.database);
    await checkTables(queryFunc, args.tables);

    const table$ = from(args.tables);

    const tableFields$ = table$.pipe(
        map(table => new Map([
            [table.fields.dtm, 'datetime'],
            ...Object.entries(table.fields.dimensions)
        ] as [string, typeof table.fields.dimensions[string] | 'datetime'][]))
    );

    const tableSelect$ = zip(table$, tableFields$).pipe(
        map(([table, fields]) => makeSelectFunc(queryFunc, {from: table.from, fields: [...fields.keys()]}))
    );

    const sources$ = zip(tableSelect$, tableFields$).pipe(
        switchMap(([select, fields]) => loadSources(fields, select))
    );
    const dimensions$ = zip(table$, sources$).pipe(
        map(([table, sources]) => {
            const dateDimension = {
                field: table.fields.dtm,
                fieldType: 'single' as const,
                value: sources.get(table.fields.dtm) as DateRange
            };

            return [
                dateDimension,
                [...sources.entries()]
                    .filter(([field, _value]) => field !== dateDimension.field)
                    .map(([field, value]) => ({
                        field,
                        value,
                        fieldType: ['int[]', 'string[]'].includes(table.fields.dimensions[field])
                            ? 'array' as const
                            : 'single' as const
                    }))

            ] as const;
        })
    );

    const tableCalc$ = zip(table$, tableSelect$).pipe(
        map(([table, select]) => makeStatsCalc(
            select,
            new Map(
                randMeasures(
                    new Map(Object.entries(table.fields.measures))
                ).map(exp => [`${exp.exp}_${exp.field}`, exp])
            )
        ))
    );

    const tableFuncTuples$ = zip(table$, tableCalc$, dimensions$).pipe(
        map(([table, calc, dimensions]) => {
            const keyFields = Object.entries(table.fields.measures)
                .filter(([_field, type]) => type === 'key')
                .map(([field]) => field);
            return [table.name, (periodInSeconds: number) => {
                return calc(randSubDimensions(dimensions[0], dimensions[1], periodInSeconds), sample(keyFields))
                    .toPromise();
            }] as const;
        }),
        toArray()
    );

    const argsTuples$ = dimensions$.pipe(
        map(dimension => dimension[0].value),
        min((dateRangeA, dateRangeB) => {
            const periodA = dateRangeA.max.getTime() - dateRangeA.min.getTime();
            const periodB = dateRangeB.max.getTime() - dateRangeB.min.getTime();
            return periodA < periodB ? -1 : 1;
        }),
        map(minDateRange => {
            const minPeriod = minDateRange.max.getTime() - minDateRange.min.getTime();

            const hourInSeconds = 60 * 60;
            const dayInSeconds = 24 * hourInSeconds;
            const monthInSeconds = 30 * dayInSeconds;
            const possibleArgsTuples: [label: string, args: [number]][] = [
                ['1 hour', [hourInSeconds]],
                ['12 hour', [12 * hourInSeconds]],
                ['1 day', [dayInSeconds]],
                ['1 week', [7 * dayInSeconds]],
                ['1 month', [monthInSeconds]],
                ['6 month', [6 * monthInSeconds]]
            ];

            return possibleArgsTuples.filter(([_, [period]]) => period <= minPeriod);
        })
    );


    const report = await zip(tableFuncTuples$, argsTuples$).pipe(
        map(([tableFuncTuples, argsTuples]) => makePerfReport('Table',
            tableFuncTuples,
            argsTuples,
            groupAvgMeasureInNanoseconds
        )),
        first()
    ).toPromise();

    for (const row$ of report) {
        await row$.pipe(
            tap(val => output.write(val.padEnd(20))),
            toArray()
        ).toPromise();
        output.write('\n');
    }
}

async function checkConnection(args: DbArguments): Promise<void> {
    try {
        await checkDb(args.host, args.port, args.user, args.password, args.database);
    } catch (e) {
        throw new Error('Не получилось подключиться к СУБД.\nПараметры:'
            + `\n\thost: ${args.host}:${args.port}`
            + `\n\tuser: ${args.user}`
            + `\n\tpassword: ${args.password}`
            + `\n\tdatabase: ${args.database}`
            + `\nИсходная ошибка:\n${JSON.stringify(e)}`);
    }
}

async function checkTables(query: QueryFunc, tables: DeepReadonly<StatsTable[]>): Promise<void> {
    for (const table of tables) {
        try {
            await checkTable(query, {
                from: table.from,
                fields: [table.fields.dtm, ...Object.keys(table.fields.dimensions)]
            });
        } catch (e) {
            throw new Error(`Ошибка при проверке таблицы:\n${JSON.stringify(table, null, '\t')}.`
                + '\nВозможно, таблица в базе не соответствует описанию.'
                + `\nИсходная ошибка:\n${JSON.stringify(e)}`);
        }
    }
}
