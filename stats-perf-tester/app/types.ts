import {FieldType as DbFieldType} from './dbs';
import {DataType as StatFieldType} from './stats';


export interface StatsTable {
    name: string;
    from: string;
    fields: {
        dtm: string;
        dimensions: Record<string, DbFieldType>;
        measures: Record<string, StatFieldType>;
    };
}
// Так же для генератора json-схемы
export type StatsTables = StatsTable[];

export interface Arguments extends DbArguments {
    tables: StatsTable[];
}

export interface DbArguments {
    host: string;
    port: number;
    user: string;
    password: string;
    database: string;
}

export interface DateRange {
    min: Date;
    max: Date;
}
