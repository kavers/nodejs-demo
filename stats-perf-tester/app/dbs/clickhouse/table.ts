import {Observable} from 'rxjs';

import {fieldToString, filterToString, FilterExp, FieldExp} from './query';

import {QueryFunc} from './types';


export function makeSelectFunc<F extends string>(query: QueryFunc, table: Table<F>): SelectFunc<F> {
    return (
        fields,
        filters,
        orders,
        groupBy,
        limit
    ) => {
        if (fields.length < 1) {
            throw new TypeError('Для выборки из таблицы ожидается хотя бы одно поле');
        }

        return query(toSql(
            fields.map(fieldToString),
            table.from,
            filters ? filters.map(filterToString).join(' AND ') : undefined,
            orders ? orders.map(exp => `${exp.field} ${exp.order}`) : undefined,
            groupBy,
            limit
        )) as Observable<Row>;
    };
}

export async function checkTable(query: QueryFunc, table: Table): Promise<void> {
    await query(
        toSql(table.fields, table.from, undefined, undefined, undefined, 1)
    ).toPromise();
}

function toSql(
    fields: readonly string[],
    from: string,
    where?: string,
    order?: readonly string[],
    groupBy?: readonly string[],
    limit?: number
): string {
    return `SELECT ${fields.join(', ')}`
        + `\nFROM ${from}`
        + (where ? `\nWHERE ${where}` : '')
        + (groupBy && groupBy.length ? `\nGROUP BY ${groupBy.join(', ')}` : '')
        + (order && order.length ? `\nORDER BY ${order.join(', ')}` : '')
        + (limit !== undefined ? `\nLIMIT ${limit}` : '');
}

interface Table<Field extends string = string> {
    from: string;
    fields: Field[];
}

type Row<Name extends string = string> = Record<Name, FieldType>

interface SelectFunc<F extends string> {
    <Alias extends string>(
        fields: readonly FieldExp<F, Alias>[],
        filters?: readonly FilterExp<F>[],
        orders?: readonly OrderExp<F>[],
        groupBy?: readonly F[],
        limit?: number
    ): Observable<Row<Alias>>;
}

interface OrderExp<Name extends string> {
    field: Name;
    order: 'asc' | 'desc';
}

type FieldType = number | string | (number | string)[] | null;
