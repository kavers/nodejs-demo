import fc, {Arbitrary} from 'fast-check';

import {FieldExp, toString} from './field';


describe('Функции для формирования списка полей в запросах', () => {
    const aggregators = ['min', 'max', 'count', 'std'] as const;

    const fieldExpArbitrary: Arbitrary<FieldExp<string, string>> = fc.record({
        name: fc.string(1, 10),
        alias: fc.string(1, 10),
        exp: fc.constantFrom(...aggregators)
    });

    describe('toString - преобразует описание поля в строку', () => {
        it('Не падает при валидных параметрах', () => {
            fc.assert(
                fc.property(fieldExpArbitrary, fieldExp => {
                    expect(toString(fieldExp)).toBeDefined();
                })
            );
        });

        it('В результате присутствует alias', () => {
            fc.assert(
                fc.property(fieldExpArbitrary, fieldExp => {
                    expect(toString(fieldExp)).toContain(fieldExp.alias);
                })
            );
        });

        it('В результате присутствует name', () => {
            fc.assert(
                fc.property(fieldExpArbitrary, fieldExp => {
                    expect(toString(fieldExp)).toContain(fieldExp.name);
                })
            );
        });
    });
});
