import fc, {Arbitrary} from 'fast-check';
import {FilterExp, toString} from './filter';


describe('Функции для работы с условиями блока WHERE в запросе', () => {
    const comparators = ['hasAny', 'in', 'between'] as const;

    const rangeArbitrary = fc.record({
        min: fc.date(),
        max: fc.date()
    });

    const filterExpArbitrary = fc.constantFrom(...comparators)
        .chain(comparator => fc.record({
            field: fc.string(1, 10),
            comparator: fc.constant(comparator),
            value: comparator === 'between' ? rangeArbitrary : fc.array(fc.integer(), 1, 100)
        })) as Arbitrary<FilterExp<string>>;

    describe('toString - преобразует описание условия в строку', () => {
        it('Не падает при валидных параметрах', () => {
            fc.assert(
                fc.property(filterExpArbitrary, filterExp => {
                    expect(toString(filterExp)).toBeDefined();
                })
            );
        });

        it('В результате присутствует имя поля', () => {
            fc.assert(
                fc.property(filterExpArbitrary, filterExp => {
                    expect(toString(filterExp)).toContain(filterExp.field);
                })
            );
        });
    });
});
