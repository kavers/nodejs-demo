export function toString(fieldExp: FieldExp<string, string>): string {
    const {name, alias, exp} = fieldExp;

    return `${exp ? expToString(exp, name) : name} ${alias}`;
}

export interface FieldExp<Name extends string, Alias extends string> {
    name: Name;
    alias: Alias;
    exp?: Aggregator;
}

function expToString(exp: Aggregator, fieldName: string): string {
    return aggregators[exp](fieldName);
}

const aggregators = {
    groupSingle: (field: string) => `groupUniqArray(${field})`,
    groupArray: (field: string) => `groupUniqArrayArray(${field})`,
    min: (field: string) => `min(${field})`,
    max: (field: string) => `max(${field})`,
    count: (field: string) => `count(${field})`,
    avg: (field: string) => `avg(${field})`,
    std: (field: string) => `stddevPop(${field})`,
    sum: (field: string) => `sum(${field})`,
} as const;

type Aggregator = keyof typeof aggregators;
