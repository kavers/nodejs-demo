export function toString(exp: FilterExp<string>): string {
    switch(exp.comparator) {
        case 'in':
            return `${exp.field} IN ${operandToString(exp.value)}`;
        case 'hasAny':
            return `hasAny(${exp.field}, ${operandToString(exp.value)})`;
        case 'between':
            return `${exp.field} BETWEEN ${operandToString(exp.value.min)} AND ${operandToString(exp.value.max)}`
    }
}

export type FilterExp<F extends string> = Filter<F, 'in' | 'hasAny', readonly (string | number)[]> | Filter<F, 'between', DateRange>;

function operandToString(val: Operand): string {
    if (val instanceof Date) {
        return `'${val.toISOString()
            .replace(/T/, ' ')
            .replace(/\..+/, '')}'`;
    } else if (isArr(val)) {
        return '[' + val
            .map(sub => operandToString(sub))
            .join(',') + ']';
    } else if (val === null) {
        return 'NULL';
    }

    switch (typeof val) {
        case 'string':
            return `'${val}'`;
        case 'number':
            return `${val}`;
    }
}

function isArr(val: Operand): val is readonly (number | string)[] {
    return val instanceof Array;
}

interface Filter<F extends string, C extends string, T extends Operand | DateRange> {
    readonly field: F;
    readonly comparator: C;
    readonly value: T;
}

interface DateRange {
    readonly min: Date;
    readonly max: Date;
}

type Operand = string | number | readonly (string | number)[] | Date;
