import {ClickHouse} from 'clickhouse';
import {from} from 'rxjs';
import {catchError, switchMap} from 'rxjs/operators';

import {QueryFunc} from './types';


export function makeQueryFunc(host: string, port: number, username: string, password: string, database: string): QueryFunc {
    const clickhouse = new ClickHouse({url: toUrl(host), port, basicAuth: {username, password}, config: {database}});

    return sql => from(clickhouse.query(sql).toPromise() as Promise<Iterable<unknown>>).pipe(
        catchError(err => {
            throw `Ошибка при забросе к базе данных: ${sql}.\nИсходная ${(err as Error).message}`;
        }),
        switchMap(rows => from(rows))
    );
}

export async function checkDb(host: string, port: number, username: string, password: string, database: string): Promise<void> {
    await (new ClickHouse({url: toUrl(host), port, basicAuth: {username, password}, config: {database}}))
        .query('SELECT 1')
        .toPromise();
}

function toUrl(host: string): string {
    if (/^https?:/.test(host)) {
        throw new TypeError('Вместо host передан уже URL');
    }

    return 'http://' + host;
}
