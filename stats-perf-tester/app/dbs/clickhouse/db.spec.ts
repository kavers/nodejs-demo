import fc from 'fast-check';

import {makeQueryFunc} from './db';


describe('Функции взаимодействия с Clickhouse', () => {
    const dbArgsArbitrary = fc.record({
        host: fc.domain(),
        port: fc.nat(65535),
        user: fc.string(1, 100),
        password: fc.string(),
        db: fc.string(1, 100)
    });

    describe('makeQueryFunc - фабричный метод для функций-запросов к базе', () => {
        it('Не падает при валидных входных параметрах', () => {
            fc.assert(
                fc.property(dbArgsArbitrary, args => {
                    expect(makeQueryFunc(args.host, args.port, args.user, args.password, args.db)).toBeDefined();
                })
            );
        });

        it('Передача URL вместо чистого host выбрасывает исключение', () => {
            fc.assert(
                fc.property(
                    fc.webUrl({validSchemes: ['https', 'http']}),
                    dbArgsArbitrary,
                    (url, args) => {
                        expect(() => makeQueryFunc(url, args.port, args.user, args.password, args.db)).toThrow();
                    }
                )
            );
        });
    });
});
