import fc, {Arbitrary} from 'fast-check';
import {from, Observable} from 'rxjs';
import {toArray} from 'rxjs/operators';

import {makeSelectFunc} from './table';

describe('Функции по работе с таблицами в базе', () => {
    type Row = Record<string, string | number>;

    const tableArbitrary = fc.record({
        from: fc.string(1, 100),
        fields: fc.array(fc.string(1, 10), 1, 10)
    });
    const queryResultArbitrary = fc.array(
        fc.object({
            maxDepth: 0,
            maxKeys: 10,
            key: fc.string(1, 100),
            values: [fc.string(), fc.integer(), fc.float()]
        }) as Arbitrary<Record<string, string | number>>,
        100
    );
    const queryFuncArbitrary = fc.func(fc.constant(new Observable<Row>()));
    const aggregators = ['sum', 'min', 'max'] as const;
    const aliasFieldArbitrary = fc.record({
        exp: fc.constantFrom(...aggregators),
        alias: fc.string(1, 10),
        name: fc.string(1, 10)
    });

    describe('makeSelectFunc - возвращает функцию для запроса отдельных полей и таблицы', () => {
        it('Отрабатывает при валидных аргументах', () => {
            fc.assert(
                fc.property(tableArbitrary, queryFuncArbitrary, (table, query) => {
                    expect(makeSelectFunc(query, table)).toBeDefined();
                })
            );
        });

        it('Функция-результат возвращает данные, которые возвращает queryFunc', async () => {
            await fc.assert(
                fc.asyncProperty(
                    tableArbitrary,
                    queryResultArbitrary,
                    fc.array(aliasFieldArbitrary, 1, 10),
                    async (table, result, fields) => {
                        const query = (): Observable<Row> => from(result);
                        const select = makeSelectFunc(query, table);
                        await expectAsync(select(fields).pipe(toArray()).toPromise()).toBeResolvedTo(result);
                    }
                )
            );
        });

        it('Функция-результат падает, если падает queryFunc', () => {
            fc.assert(
                fc.property(
                    tableArbitrary,
                    fc.array(aliasFieldArbitrary, 1, 10),
                    (table, fields) => {
                        const query = (): never => { throw new Error(); };
                        const select = makeSelectFunc(query, table);
                        expect(() => select(fields)).toThrow();
                    }
                )
            );
        });

        it('Функция-результат падает, если не переданы поля в аргументах', () => {
            fc.assert(
                fc.property(
                    tableArbitrary,
                    queryResultArbitrary,
                    (table, result) => {
                        const query = (): Observable<Row> => from(result);
                        const select = makeSelectFunc(query, table);
                        expect(() => select([])).toThrow();
                    }
                )
            );
        });
    });
});
