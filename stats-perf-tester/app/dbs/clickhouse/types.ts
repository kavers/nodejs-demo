import {Observable} from 'rxjs';

export interface QueryFunc {
    (sql: string): Observable<unknown>;
}
