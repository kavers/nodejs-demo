import {map} from 'rxjs/operators';

import {DateRange, SelectFunc} from './types';


export async function loadSources<F extends string>(
    fields: ReadonlyMap<F, FieldType>,
    select: SelectFunc<F>
): Promise<Map<F, SourceType>> {
    const sources = new Map<F, SourceType>();
    for (const [field, fieldType] of fields.entries()) {
        switch (fieldType) {
            case 'datetime':
                sources.set(field, await loadDateRange(select, field));
                break;
            case 'string':
            case 'int':
                sources.set(field, new Set(await loadSingleUniq(select, field)));
                break;
            case 'string[]':
            case 'int[]':
                sources.set(field, new Set(await loadArrUniq(select, field)));
        }
    }

    return sources;
}

export type FieldType = 'datetime' | 'int' | 'string' | 'int[]' | 'string[]';

type SourceType = DateRange | Set<string | number>;

async function loadDateRange<F extends string>(select: SelectFunc<F>, field: F): Promise<DateRange> {
    return select([
        {alias: 'min', name: field, exp: 'min'},
        {alias: 'max', name: field, exp: 'max'}
    ]).pipe(
        map(range => {
            if (typeof range.min !== 'string' || typeof range.max !== 'string') {
                throw new TypeError(
                    `Unexpected data from datetime column. Waits for strings, but got ${JSON.stringify(range.min)} and ${JSON.stringify(range.max)}`
                );
            }
            return {min: new Date(range.min), max: new Date(range.max)};
        })
    ).toPromise();
}

async function loadArrUniq<F extends string>(select: SelectFunc<F>, field: F): Promise<(number | string)[]> {
    return select([{alias: 'src', name: field, exp: 'groupArray'}]).pipe(
        map(res => res.src as (number | string)[])
    ).toPromise();
}

async function loadSingleUniq<F extends string>(select: SelectFunc<F>, field: F): Promise<(number | string)[]> {
    return select([{alias: 'src', name: field, exp: 'groupSingle'}]).pipe(
        map(res => res.src as (number | string)[])
    ).toPromise();
}
