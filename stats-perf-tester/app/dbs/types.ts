import {Observable} from 'rxjs';

export interface QueryFunc {
    (sql: string): Observable<unknown>;
}

export interface SelectFunc<F extends string> {
    <Alias extends string>(
        fields: readonly FieldExp<F, Alias>[],
        filters?: readonly FilterExp<F>[],
        orders?: readonly OrderExp<F>[],
        groupBy?: readonly F[],
        limit?: number
    ): Observable<Row<Alias>>;
}

export interface DateRange {
    readonly min: Date;
    readonly max: Date;
}

interface FieldExp<Name extends string, Alias extends string> {
    name: Name;
    alias: Alias;
    exp?: 'min' | 'max' | 'groupSingle' | 'groupArray' | 'count' | 'avg' | 'std' | 'sum';
}

type FilterExp<F extends string> = Filter<F, 'in', readonly (string | number)[]> | Filter<F, 'between', DateRange>;

interface OrderExp<Name extends string> {
    field: Name;
    order: 'asc' | 'desc';
}

type Row<Name extends string = string> = Record<Name, number | string | (number | string)[] | null>

interface Filter<F extends string, C extends string, T extends readonly (string | number)[] | DateRange> {
    readonly field: F;
    readonly comparator: C;
    readonly value: T;
}
