import fc from 'fast-check';

import {loadSources} from './source';


describe('Функции для загрузки данных из таблицы в наборы и работы с ними', () => {
    describe('loadSources - загрузка данных из таблицы по предопределённым колонкам в наборы', () => {
        const sourceFieldType = ['datetime', 'int', 'string', 'int[]', 'string[]'] as const;
        const fieldsArbitrary = fc.array(
            fc.tuple(fc.string(1, 10), fc.constantFrom(...sourceFieldType)),
            1, 10
        );

        it('Падаем на исключение в select', async () => {
            await fc.assert(
                fc.asyncProperty(fieldsArbitrary, fc.string(1, 10), async (fields, msg) => {
                    const select = (): never => {
                        throw new Error(msg);
                    }

                    await expectAsync(loadSources(new Map(fields), select)).toBeRejectedWithError(msg);
                })
            );
        });
    });
});
